---
layout: handbook-page-toc
title: Package Group - GitLab Quality Assurance End-to-End Testing for the Package group
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Overview

The goal of this page is to document how the package group uses the [GitLab QA framework](https://gitlab.com/gitlab-org/gitlab-qa) ([video walkthrough](https://www.youtube.com/watch?v=eP1esI-o_0o)) to implement and run [end-to-end tests](https://docs.gitlab.com/ee/development/testing_guide/end_to_end/).

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/PHvZxvHxwXw" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

[Supporting slides for the above tutorial](https://docs.google.com/presentation/d/1CIovqR0iNPjr8lGhwvFl2b69ZGbEgd2X5zn_TOu_khY/edit?usp=sharing)

### Why do we have them

> End-to-end testing is a strategy used to check whether your application works as expected across the entire software stack and architecture, including the integration of all micro-services and components that are supposed to work together.

This is particularly true for a group that works with several services such as the [Container Registry](https://about.gitlab.com/direction/package/#container-registry), and the uploading of packages to a [Package Registry](https://about.gitlab.com/direction/package/#package-registry). These tests use the software as a user would and without mocking component dependencies.
The testing strategy for [this level of the pyramid](https://docs.gitlab.com/ee/development/testing_guide/testing_levels.html) can be found under the [Package QA Test Suite](https://gitlab.com/groups/gitlab-org/-/epics/5082) epic.

### When and where do we run them

**Any time** - This can be done either locally in [GDK](https://gitlab.com/gitlab-org/gitlab-development-kit/-/blob/master/README.md), using the gitlab-qa gem or by triggering a pipeline on a specific environment (Staging, Nightly, Canary, etc).

**Merge Request** - The whole QA End-to-End test suite can be run on your MR by triggering manually the `package-and-qa` job.

**Scheduled Pipelines** - [Schedule](https://about.gitlab.com/handbook/engineering/quality/quality-engineering/debugging-qa-test-failures/#scheduled-qa-test-pipelines).
Package tests run in all the scheduled pipelines since we have a few tests tagged as `:reliable`. These tests (`:reliable` and `:smoke`) block deployments in case of failure and are a part of GitLab's sanity test suite.
Other Package related tests that are not tagged as `:reliable` run when the full suite of tests runs.


### Where are they

In the [GitLab repository](https://gitlab.com/gitlab-org/gitlab), the End-to-End tests for the Package group are located at:
- `qa/qa/specs/features/api/5_package` _*_
- `qa/qa/specs/features/browser_ui/5_package`
- `qa/qa/specs/features/ee/api/5_package` _*_
- `qa/qa/specs/features/ee/browser_ui/5_package` _*_

_* There are currently no API tests or browser UI tests for paid features (Enterprise Edition)._

### How to run them locally

#### GDK

To test against your local GDK, first make sure:
- Environment variables are correctly set
	- `QA_DEBUG` is set to **true** so the debug logs are enabled
	- `WEBDRIVER_HEADLESS` is set to **false** so you can see the test run in an automated browser
- GDK is up and running
	- and using a [loopback interface](https://gitlab.com/gitlab-org/gitlab-development-kit/-/blob/master/doc/index.md#create-loopback-interface-for-gdk) to be able to use a runner in a docker container
	- [hostname mapped to the loopback interface](https://gitlab.com/gitlab-org/gitlab-development-kit/-/blob/master/doc/index.md#set-up-gdktest-hostname)

To run the tests:
1. On the terminal, go to `path-to-your-gdk/gitlab/qa`
1. Make sure that you have all the necessary gems installed: `bundle install`
1. Issue the command:
	1. To run all the tests for free features: `bundle exec bin/qa Test::Instance::All http://gdk.test:3000 -- qa/specs/features/browser_ui/5_package --tag orchestrated --tag packages`
	1. To run all the tests for paid features: `bundle exec bin/qa Test::Instance::All http://gdk.test:3000 -- qa/specs/features/ee/browser_ui/5_package --tag orchestrated --tag packages` (currently there are no tests for paid features)
	1. To run all the API tests for free features: *currently there are API tests for free features **at the End-to-End level** but they run only in Staging and Preprod environments*
	1. To run all the API tests for free features: *currently there are no API tests for paid features **at the End-to-End level***

**Note:** The command above is targeting `http://gdk.test:3000` which should be changed according to your hostname mapped to the loopback interface.

- `Test::Instance::All` refers to the test scenario `Instance::All`. A test scenario is a statement describing the functionality of the application to be tested.
These [are created on the GitLab QA](https://gitlab.com/gitlab-org/gitlab-qa/-/tree/master/lib/gitlab/qa/scenario/test) orchestration tool to define and compose
all the necessary pre-conditions that a GitLab instance must have in order to be tested. `All` is just simply running all the tests without pre-configuring a GitLab
instance as we are using our GDK as the GitLab instance under test. In order to configure the GDK instance to have the Registry and/or the Package Registry enabled please follow the existing [GDK Docs](https://gitlab.com/gitlab-org/gitlab-development-kit/-/tree/main/doc/howto).
- `http://gdk.test:3000` is the hostname of the GitLab instance under test. When running locally is the hostname of the GDK.
- `qa/specs/features/browser_ui/5_package` is the path to the folder where non-paid package features are.
- `--tag orchestrated --tag packages` are [RSpec metadata](https://docs.gitlab.com/ee/development/testing_guide/end_to_end/rspec_metadata_tests.html#rspec-metadata-for-end-to-end-tests) used for filtering tests.
Particularly useful when running on pipelines, but they also need to be included when running locally since they act as a filter for running `:packages` related tests only.


#### Using the gitlab-qa gem

Install the `gitlab-qa` gem by running the command `gem install gitlab-qa`. This gem was created to verify the integrated pieces that compose GitLab work well together making it possible to run the QA test suite for any merge request before it gets merged to master.
To run orchestrated-level scenarios related to the Package group:
* Run the command: `gitlab-qa Test::Instance::Image EE --omnibus-config packages` to test `:packages` tagged tests against an Omnibus instance configured [to have the Package Registry enabled.](https://gitlab.com/gitlab-org/gitlab-qa/-/blob/master/lib/gitlab/qa/runtime/omnibus_configurations/packages.rb)
* Run the command: `gitlab-qa Test::Instance::Image EE --omnibus-config object_storage` to test `:object_storage` tagged tests (all package managers) against an Omnibus instance [configured to use Minio as the storage dafault](https://gitlab.com/gitlab-org/gitlab-qa/-/blob/master/lib/gitlab/qa/runtime/omnibus_configurations/object_storage.rb), instead of local storage.
* Run the command: `GITLAB_TLS_CERTIFICATE=$(cat /path/to/certificate.crt) gitlab-qa Test::Integration::Registry EE` to test `:registry` tagged tests against an Omnibus instance [configured to use a Registry with TLS](https://gitlab.com/gitlab-org/gitlab-qa/-/blob/master/lib/gitlab/qa/scenario/test/integration/registry.rb).

`EE` is the `latest` tagged image for [gitlab-ee](https://hub.docker.com/r/gitlab/gitlab-ee/).

More information on how to run tests using the gitlab-qa gem can be found on [What Tests Can Be Run](https://gitlab.com/gitlab-org/gitlab-qa/-/blob/master/docs/what_tests_can_be_run.md) docs on the GitLab QA repository.

### FAQ

#### Can I get a GITLAB_TLS_CERTIFICATE to run Container Registry tests locally?
Yes. This can be found on the [tls_certificates folder in GitLab QA](https://gitlab.com/gitlab-org/gitlab-qa/-/blob/master/tls_certificates/gitlab/gitlab.test.crt).

#### I triggered package-and-qa. Where do I find the tests?
If you have an MR and want to make sure it is running the End-to-End tests, please trigger the manual `package-and-qa` job on the pipeline of your MR. After the pipeline runs there will be a note on your MR titled "Allure report" with a `package-and-qa` test report link. 
It is also possible to see which jobs failed in the `package-and-qa` pipeline, just follow the downstream pipelines, and within the `gitlab-qa-mirror` pipeline, access the `packages` job to inspect the result. We also have relevant Package tests running in `object_storage` and `registry` jobs.

In Staging, or other environments [that run full tests](https://about.gitlab.com/handbook/engineering/quality/quality-engineering/debugging-qa-test-failures/#scheduled-qa-test-pipelines), all the
tests within the `qa/specs/features/browser_ui/5_package` folder can be found running on the `qa-triggers-browser_ui-5_package` job.

#### What are orchestrated-level scenarios and which do we have available for the Package group?
Orchestrated-level scenarios are configurations for a GitLab instance and all necessary components required to perform the test. An example would be a GitLab instance with [object storage settings](https://gitlab.com/gitlab-org/gitlab-qa/-/blob/master/lib/gitlab/qa/runtime/omnibus_configurations/object_storage.rb) enabled, using [a Minio component](https://gitlab.com/gitlab-org/gitlab-qa/-/blob/master/lib/gitlab/qa/component/minio.rb).
Instance-level scenarios are the package tests scripts in the `qa/qa/specs/` folder.

* The Packages scenario (runs tests on a job named `packages`) configures a GitLab Omnibus instance to have the Package Registry explicitly enabled.
* The Object Storage scenario (runs tests on a job named `object_storage`) runs the all the package managers tests using Minio as the default storage option instead of local storage.
* The Registry scenario (runs tests on a job named `registry`) runs the Container Registry test(s) for Omnibus GitLab, configuring a Registry using TLS.

### Troubleshooting
Please reach out to [your counterpart SET](https://about.gitlab.com/handbook/engineering/quality/#individual-contributors) or in the `#quality` channel.

### Helpful Documentation

- [Testing Guide - End-to-End Testing](https://docs.gitlab.com/ee/development/testing_guide/end_to_end/)
- [GitLab QA orchestration tool](https://gitlab.com/gitlab-org/gitlab-qa)
- [Run QA tests against your GDK setup](https://gitlab.com/gitlab-org/gitlab-qa/-/blob/master/docs/run_qa_against_gdk.md)
- [Beginner's Guide to writing End-to-End tests](https://docs.gitlab.com/ee/development/testing_guide/end_to_end/beginners_guide.html)
