---
layout: job_family_page
title: "Talent Brand and Talent Acquisition Enablement"
---

## Talent Brand 

## Levels
 
### Talent Brand Program Manager
 
The Talent Brand Program Manager reports to the Senior Director of Talent Brand & Talent Acquisition Enablement. 
 
#### Talent Brand Program Manager Job Grade
 
The Talent Brand Program Manager is a [grade 6](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).
 
#### Talent Brand Program Manager Responsibilities
 
- Build talent brand programs, operationalize them, and measure their success
- Collaborate with key stakeholders to develop GitLab's talent brand
- Monitor and grow our brand presence across key talent-facing channels (jobs site, LinkedIn, Glassdoor, etc.)
- Partner with the People Operations team and key stakeholders globally to develop and execute strategies that enhance GitLab’s visibility in key recruiting markets
- Evolve GitLab’s talent brand strategy to effectively articulate GitLab’s culture and value proposition to attract top talent
- Provide strategy and support for related programs such as GitLab's diversity, inclusion and belonging
- Collaborate with internal GitLab teams, particularly Marketing, on key projects (social media, jobs site, events, remote work, PR, internal communications) to ensure consistency in messaging and approach
- Develop recruitment marketing campaigns to increase awareness of GitLab's talent brand with potential candidates and help the Recruiting team meet hiring plan goals
- Enhance the look and feel of communications (internal and external) throughout the candidate and team member lifecycle
- Define our approach for employer awards and lists that will help GitLab be recognized as an employer of choice; complete applications and manage related internal surveys for these awards
- Create recruiter enablement tools to assist the recruiting team in sharing the GitLab talent brand with candidates
 
### Talent Brand Program Manager Requirements
 
- Bachelor’s degree and 3+ years of Marketing or Human Resources experience
- Prior marketing, brand, and social media experience (preferably within recruiting or talent brand)
- Demonstrated ability to deliver targeted recruitment marketing, social media, and employee value proposition building strategies
- A natural storyteller with excellent narration and writing skills
- Ability to navigate cultural differences and build global but locally relevant solutions
- Strong social and communication skills (verbal and written), across all levels
- Excellent organizational skills, time management, and priority setting
- Deadline oriented, and able to work in a fast-paced environment with ever-changing priorities
- Self-motivated with the ability to work both independently and collaboratively
- Proficient in Google Docs
- You share our [values](/handbook/values/), and work in accordance with those values
- Successful completion of a [background check](/handbook/legal/gitlab-code-of-business-conduct-and-ethics/#background-checks)
- Ability to use GitLab

## Enablement

## Levels 

### Program Manager, Enablement

The Program Manager, Enablement reports to the Manager, Global Enablement.

#### Program Manager, Enablement Job Grade

The Progam Manager, Enablement is a [grade 6](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

#### Program Manager, Enablement Responsibilities

The Program Manager, Enablement Responsibilities fall into four distinct buckets:

* Operations
   * Work with the Manager, Enablement and systems/tools team to develop bespoke reporting for the TA function
   * Work with the Manager, Enablement on current offer process and general process workflow within Greenhouse and how it reflects in our reporting
   * Ensure that our escalation processes and customer-facing Slack channels are managed with established SLA’s, style and communication protocols
   * Understand end-to-end TA processes and employ a “continuous improvement & iteration” rhythm 
   * Manage our procurement issues and certain external vendor relationships as it pertains to billing, action planning etc
* Learning & Development
   * Partner with TA leadership to develop the GitLab TA playbook (pitch decks, intake sessions, closing styles) and the GitLab candidate assessment framework
   * Own the recruiting team onboarding program
   * Develop learning paths for TA by level
   * Partner with the Head of TA to define a level-based competency framework
   * Partner with the Head of TA on our promotions cycle
* Communications 
   * Own all regular decks for Global Town Halls, defining and collating content, driving the meetings
   * Partner with Head of TA and Head of TA Brand and Enablement on internal comms cadences and vehicles 
   * Be an escalation point for the TA Leadership team and be comfortable triaging issues for rapid resolution
* Strategy
   * Be a thought-partner for the TA leadership team on operational and enablement issues
   * Play a part in future growth conversations, develop a strong relationship with finance to be able to understand the future plan for the org and how TA can play its part it fuelling the growth of the organization 
   * With a continued focus on Diversity, inclusion and belonging, the Enablement Analyst will ensure inclusivity as we establish processes and policies for our TA team members and candidates alike.

#### Program Manager, Enablement Requirements

* Experience working in a fast-paced environment
* A strong track record of successful project execution and strong project management skills
* Strong organizational skills
* Strong customer service and communication skills
* Change Management experience
* Ability to understand a high-level issue and conduct root cause analysis to understand issues
* Ability to use GitLab
* Aligns with GitLab values 

### Manager, Global Enablement

The Manager, Global Enablement reports to the Director of Talent Brand & Talent Acquisition Enablement. 

#### Manager, Global Enablement Job Grade

The Manager, Recruiting Operations is a [grade 8](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

#### Manager, Global Enablement Responsibilities

* Design and drive rigour in processes and policies for our Global Talent Acquisition team
* Partner with our tools team to optimize our systems, tools and processes to create consistent, efficient and equitable hiring outcomes 
* Partner with finance and our PBP team to ensure that the necessary process flows are built for scale
* Partner with our Data and Analytics team to build a solid reporting suite for the Head of TA, their leadership team and the hiring manager community
* Provide coaching to improve performance of team members and drive accountability
* Onboard, mentor, and grow the careers of all team members
* Develop, own and run the GItlab hiring planning process in collaboration with the various operations teams, finance and the PBP’s
* Build trusted partnerships within GitLab and externally to collaborate, drive alignment and stay on top of trends
* Leverage our candidate facing teams to advocate for our employer brand and deliver a world-class experience
* Help define and leverage data focused metrics in order to develop and deliver on the goals

#### Manager, Global Enablement Requirements

* Exceptional cross-functional communication and organization skills, and demonstrated experience in time-management and ability to influence
* A creative mindset to get things done effectively and efficiently
* Familiarity with an ATS and other tools
* History of influence over a team
* Robust time management, communication and organizational skills
* A team player with excellent client management skills
* Ability to use GitLab

#### Manager, Global Enablement Performance Indicators

* [Average candidate ISAT](/handbook/hiring/metrics/#interviewee-satisfaction-isat)
* [Hires vs. Plan](/handbook/hiring/metrics/#hires-vs-plan)
* [Time to Offer Accept](/handbook/hiring/metrics/#time-to-offer-accept-days)

## Talent Brand and Talent Acquisition Enablement Leadership

## Levels

### Director of Talent Brand & Talent Acquisition Enablement
 
The Director of Talent Brand & Talent Acquisition Enablement reports to the [VP of Talent Acquisition](/job-families/people-ops/talent-acquisition-leadership/).
 
#### Director of Talent Brand & Talent Acquisition Enablement Job Grade
 
The Director Talent Brand is a [grade 10](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).
 
#### Director of Talent Brand & Talent Acquisition Enablement Responsibilities
 
- Establish a 3-year global talent branding/marketing strategy and position talent brand as a
core priority for Global Talent Acquisition and the recruiting community at Gitlab.
- Overall responsibility for continuing and building upon the Gitlab global talent brand to
position us as an employer of choice across all functions
- Establish the talent value proposition; partnering internally to ensure a consistent experience
throughout the employee lifecycle
- Manage and own external vendors and award-body relationships. Ensuring that we are well
positioned and intentional about the work with our external partners is a key part of this role
(Glassdoor, Builtin, LinkedIN, GPTW, Forbes, Fortune etc)
- Establish a recruiter talent brand “toolkit”
- Run the talent brand portfolio to allow for both the proactive and reactive work so as to
maximize opportunities as they arise
- Partner with the data &amp; analytics function to monitor ROI on initiatives and Talent Community
work on an ongoing basis but also apply a data-mindset to all talent branding work that is
undertaken
 
#### Director of Talent Brand & Talent Acquisition Enablement Requirements
 
- Experience having built and executed upon a global marketing plan (talent or otherwise)
- Experience working across different hiring cultures and communities and comfortable working
in a truly global role
- Leadership experience (both direct and indirect)
- You share our values, and work in accordance with those values
- Excellent organizational, task, and time management skills, with an emphasis on detail and
follow up, plus the ability to prioritize tasks and requests from multiple stakeholders and
audiences
- Demonstrated ability to communicate and collaborate with individuals at all levels of the
organization with excellent customer service skills
- Ability to work in a fast-paced environment with demonstrated ability to coordinate multiple
projects/initiatives simultaneously while meeting deadlines and business objectives
- Exhibit strong data skills and ability to utilize Google Sheets
- Ability to use, or willingness to learn, GitLab
- Successful completion of a background check
- Comfortable in a spokesperson role, ideally with media training/ coaching abilities
- Experience working with various virtual meeting management tools such as Zoom is a plus
- Experience in successfully prioritizing, managing multiple projects, and working in a team
environment

### Senior Director of Talent Brand & Talent Acquisition Enablement
 
The Senior Director of Talent Brand & Talent Acquisition Enablement reports to the [VP of Talent Acquisition](/job-families/people-ops/talent-acquisition-leadership/).
 
#### Senior Director of Talent Brand & Talent Acquisition Enablement Job Grade
 
The Senior Director Talent Brand is a [grade 11](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).
 
#### Senior Director of Talent Brand & Talent Acquisition Enablement Responsibilities

* Establish a 3-year global talent branding/marketing & enablement strategy and position talent brand & enablement as a core priority for Global Talent Acquisition and the recruiting community at Gitlab.
* Overall responsibility for continuing and building upon the Gitlab global talent brand to position us as an employer of choice across all functions
* Establish the talent value proposition; partnering internally to ensure a consistent experience throughout the employee lifecycle
* Manage and own external vendors and award-body relationships. Ensuring that we are well positioned and intentional about the work with our external partners is a key part of this role (Glassdoor, Builtin, LinkedIN, GPTW, Forbes, Fortune etc)
* Establish a recruiter talent brand “toolkit”
* Run the talent brand portfolio to allow for both the proactive and reactive work so as to maximize opportunities as they arise
* Partner with the data & analytics function to monitor ROI on initiatives and Talent Community work on an ongoing basis but also apply a data-mindset to all talent branding work that is undertaken
* Be comfortable building a multi-faceted strategy that aligns our candidate experience expectations with our talent branding strategy.

#### Senior Director of Talent Brand & Talent Acquisition Enablement Requirements

* Experience having built and executed upon a global marketing plan (talent or otherwise)
* Experience working across different hiring cultures and communities and comfortable working in a truly global role
* Leadership experience (both direct and indirect)
* You share our values, and work in accordance with those values
* Excellent organizational, task, and time management skills, with an emphasis on detail and follow up, plus the ability to prioritize tasks and requests from multiple stakeholders and audiences
* Demonstrated ability to communicate and collaborate with individuals at all levels of the organization with excellent customer service skills
* Ability to work in a fast-paced environment with demonstrated ability to coordinate multiple projects/initiatives simultaneously while meeting deadlines and business objectives
* Exhibit strong data skills and ability to utilize Google Sheets
* Ability to use, or willingness to learn, GitLab
* Comfortable in a spokesperson role, ideally with media training/ coaching abilities
* Experience working with various virtual meeting management tools such as Zoom is a plus
* Experience in successfully prioritizing, managing multiple projects, and working in a team environment

## Performance Indicators
 
- [Glassdoor engagement](/handbook/people-group/employment-branding/#glassdoor-engagement)
- [LinkedIn Talent Brand metrics](/handbook/people-group/employment-branding/#linkedin-talent-brand-metrics)
- [Team member engagement score](/handbook/people-group/employment-branding/#team-member-engagement-score)
- [Team member voluntary turnover](/handbook/people-group/people-group-metrics/#team-member-voluntary-turnover)
- [Hires vs. plan](/handbook/hiring/metrics/#hires-vs-plan)
- [Social Referrals](https://about.gitlab.com/handbook/hiring/metrics/#social-referrals)
 
## Career Ladder
 
The next step in the Talent Brand job family is to move a to management role which is not yet defined at GitLab.
 
## Hiring Process
 
Applicants for this position can expect the hiring process to follow the order below. Please keep in mind that applicants can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find their job title on our [team page](https://about.gitlab.com/company/team/).
 
* Selected candidates will be invited to schedule a 30min screening call with one of our Global Recruiters
* Next, candidates will be invited to schedule a first interview with the Hiring Manager
* Next, candidates will be invited to interview with 2-5 team members
* There may be a final executive interview
 
Additional details about our process can be found on our [hiring page](https://about.gitlab.com/handbook/hiring/).
