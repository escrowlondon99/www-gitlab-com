---
layout: markdown_page
title: "FedRAMP Execution Working Group"
description: "The charter of this working group is to drive execution of FedRAMP compliance."
canonical_path: "/company/team/structure/working-groups/fedramp-execution/"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Attributes

| Property        | Value             |
|-----------------|-------------------|
| Date Created    | November 25, 2020 |
| End Date        | TBD               |
| Slack           | [#wg_fedramp](https://gitlab.slack.com/archives/C0110E0NMT9) (only accessible from within the company) |
| Google Doc      | Search "FedRAMP Working Group Agenda" in Google Drive (only accessible from within the company) |
| Issue Board     | TBD             |

### Exit Criteria

This working group will organize all the domain experts needed, surface critical decisions, centralize status, and drive execution. More specific exit criteria will be developed early in the process.

## Roles and Responsibilities

| Working Group Role             | Team Member     | Functional Title                           |
|--------------------------------|-----------------|--------------------------------------------|
| Facilitator                    | Johnathan Hunt  | VP of Security                             |
| Functional Lead: PM            | Josh Lambert    | Director of Product Management, Enablement |
| Functional Lead: Pub Sec       | Bob Stevens     | Area Vice President, Public Sector Sales   |
| Functional Lead: Compliance    | Julia Lake      | Director, Security Assurance               |
| Executive Stakeholder          | Eric Johnson    | Chief Technology Officer                   |
| Member                         | Jim Riley       | Federal ASM, Public Sector Sales           |
| Member                         | Chris Maurer    | Public Sector Manager, Customer Success    |
